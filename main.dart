void main() {
  String name = "Eliel";
  String lastname = "Loreto";

  /*
   * final = const
   */
  final int age = 26;
  final double sales = 4787.45;

  wellcome(
      name: name, lastname: lastname, age: age, sales: sales, code_cu: "USD");

  name = "$name Alejandro";

  List<int> number = [2, 4, 521, 2, 6, 0, 24, 6];

  bool? hasAdd = null;

  if (hasAdd != null) {
    number.add(45);
  } else {
    print("No se agregado ningun elemento");
  }

  Map<String, dynamic> person = {"name": name, "age": age, "number": number};

  person.addAll({"lastname": lastname});

  print("person ~ $person");

  print("main ~ number ~ $number");
  print("main ~ number ~ first item ~ ${number[0]}");

  for (int i = 0; i < number.length; i++) {
    print("main ~ for ~ number ~ item ~ ${number[i]}");
  }

  final json = {"model": "Ferrari", "isActive": true};

  final franchescoBeguri = new Car.fromJson(json);

  final rayoMcqueen = new Car(model: "BMW", isActive: true);

  franchescoBeguri.setIsActive = false;

  print("rayoMcqueen ~ $rayoMcqueen");
  print(
      "franchescoBeguri ~ model: ${franchescoBeguri.getModel}, activo: ${franchescoBeguri.getIsActive}");

  final superMan = new Hero("Clark Kent");
  final luthor = new Villain("Lex Luthor");

  print("Super Man ~ $superMan ~ ${superMan.value}");
  print("Luthor ~ $luthor ~ ${luthor.evil}");

  final flipper = new Dolphin();
  final batman = new Bat();

  flipper.swimmer();
  batman.fly();
  batman.walking();

  print("Iniciando la petición");
  httpGet("http://localhost:8080/api").then((trae) {
    print(trae);
  });

  bye(message: "Hasta luego,", fullname: name + ' ' + lastname);
}

Future httpGet(String url) {
  return Future.delayed(Duration(seconds: 2), () {
    return "Hola mundo 2 segundos";
  });
}

void wellcome(
    {required String name,
    required String lastname,
    required int age,
    required double sales,
    String code_cu = "PEN"}) {
  print("Hola $name $lastname ~ Edad: $age ~ Sueldo: ${sales + 2} $code_cu");
}

void bye({required String message, required String fullname}) {
  print("$message $fullname");
}

abstract class Vehicle {
  String model;
  bool isActive;

  Vehicle({required this.model, required this.isActive});
}

class Car implements Vehicle {
  String model;
  bool isActive;

  get getModel {
    return this.model;
  }

  set setModel(String model) {
    this.model = model;
  }

  get getIsActive {
    return this.model;
  }

  set setIsActive(bool isActive) {
    this.isActive = isActive;
  }

  Car({required this.model, required this.isActive});

  Car.fromJson(Map<String, dynamic> json)
      : this.model = json['model'],
        this.isActive = json['isActive'];

  @override
  String toString() {
    return "Modelo: ${this.model}, Activo: ${this.isActive}";
  }
}

class Airplane implements Vehicle {
  String model;
  bool isActive;

  Airplane({required this.model, required this.isActive});

  @override
  String toString() {
    return "Modelo: ${this.model}, Activo: ${this.isActive}";
  }
}

abstract class Character {
  String? power;
  String name;

  Character({required this.name});

  @override
  String toString() {
    return "$name - $power";
  }
}

class Hero extends Character {
  int value = 100;

  Hero(String name) : super(name: name);
}

class Villain extends Character {
  int evil = 100;

  Villain(String name) : super(name: name);
}

abstract class Animal {}

abstract class Mammal extends Animal {}

abstract class Bird extends Animal {}

abstract class Fish extends Animal {}

abstract class Flyer {
  void fly() => print("Flyer ~ Volando");
}

abstract class Walking {
  void walking() => print("Walking ~ Caminando");
}

abstract class Swimmer {
  void swimmer() => print("Swimmer ~ Nadando");
}

class Dolphin extends Mammal with Swimmer {}

class Bat extends Mammal with Flyer, Walking {
  @override
  void walking() {
    print("Bat ~ walking ~ Caminando");
  }
}
